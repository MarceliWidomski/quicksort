//============================================================================
// Name        : quicksort.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int size();
char name();
void fillArray(int size, int array[], char name = ' ');
void printArray(int size, int array[], char name = ' ');
void swap(int &a, int &b);
int partition(int array[], int left, int right);
void quicksort(int array[], int left, int right);

int main() {
	int maxSize(100);
	int array[maxSize] = { };
	char arrayName;
	arrayName = name();
	int arraySize;
	arraySize = size();
	fillArray(arraySize, array, arrayName);
	printArray(arraySize, array, arrayName);
	quicksort(array, 0, arraySize - 1);
	printArray(arraySize, array, arrayName);
	return 0;
}
int size() { // inputs size of array
	int size;
	cout << "Enter number of elements which are to be sorted: ";
	cin >> size;
	return size;
}
char name() { // inputs name of array
	char name;
	cout << "Enter array name: " << endl;
	cin >> name;
	return name;
}
void fillArray(int size, int array[], char name) { // fills array with elements entered by user
	for (int i = 0; i < size; ++i) {
		cout << "Enter element with index " << name << " " << i << " " << ": ";
		cin >> array[i];
	}
	cout << endl;
}
void printArray(int size, int array[], char name) { // prints array
	cout << "Array " << name << " :" << endl;
	for (int i = 0; i < size; ++i)
		cout << array[i] << " ";
	cout << endl;
}
void swap(int &a, int &b) { //swaps two integers
	a = a - b;
	b = b + a;
	a = b - a;
}
int partition(int array[], int left, int right) { //divides array by 2 parts,
	int pivot (array[left]);					//in first numbers are less than or equal to pivot
	int i(left);								// in second numbers are greater than or equal to pivot
	int j(right);								// left - lowest array index
	while(true){								// right - highest array index
	while (array[i]<pivot)
		++i;
	while (array[j]>pivot)
		--j;
	if (i>=j)
		return j;								//returns from infinite loop, j is point of array division
	swap (array[i], array[j]);					// when conditions in both loops(while) are false array element are swapped
	++i;
	--j;
	}
}
void quicksort(int array[], int left, int right) {
	int partitionPoint;
	if (left < right) {
		partitionPoint = partition(array, left, right); // divides array by 2 smaller parts
		quicksort(array, left, partitionPoint); //recursive call of quicksort for left part of array
		quicksort(array, partitionPoint + 1, right);//recursive call of quicksort for right part of array
	}
}
